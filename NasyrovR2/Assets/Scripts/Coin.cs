using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
   private void OnTriggerEnter (Collider collsion)
    {
        if (collsion.gameObject.tag == "Player")
        {
            Destroy(gameObject);
        }
    }
}
